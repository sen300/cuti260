package com.xsis.cuti260.models;

import javax.persistence.*;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "jenis_cuti")
@Where(clause = "is_delete = false")
public class JenisCuti extends Common {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "nama")
    private String nama;

    @Column(name = "keterangan", nullable = true)
    private String keterangan;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
