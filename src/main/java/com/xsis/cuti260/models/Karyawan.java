package com.xsis.cuti260.models;

import javax.persistence.*;

import org.hibernate.annotations.Where;

@Entity
@Table(
        name = "karyawan"

)
@Where(clause = "is_delete = false")
public class Karyawan extends Common {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long Id;

    @Column(name = "nama", nullable = false)
    private String Nama;

    @Column(name = "email", nullable = false)
    private String Email;

    @Column(name = "password", nullable = false)
    private String Password;

    @ManyToOne
    @JoinColumn(name = "jenis_karyawan_id", insertable = false, updatable = false)
    public JenisKaryawan jenisKaryawan;

    @Column(name = "jenis_karyawan_id", nullable = false)
    private Long JenisKaryawanId;

    @Column(name = "sisa_cuti", nullable = false)
    private Integer SisaCuti = 12;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public JenisKaryawan getJenisKaryawan() {
        return jenisKaryawan;
    }

    public void setJenisKaryawan(JenisKaryawan jenisKaryawan) {
        this.jenisKaryawan = jenisKaryawan;
    }

    public Long getJenisKaryawanId() {
        return JenisKaryawanId;
    }

    public void setJenisKaryawanId(Long jenisKaryawanId) {
        JenisKaryawanId = jenisKaryawanId;
    }

    public Integer getSisaCuti() {
        return SisaCuti;
    }

    public void setSisaCuti(Integer sisaCuti) {
        SisaCuti = sisaCuti;
    }
}
