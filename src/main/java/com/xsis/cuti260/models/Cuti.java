package com.xsis.cuti260.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Entity
@Table(name = "cuti")
@Where(clause = "is_delete = false")
public class Cuti extends Common {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long Id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.DATE)
    @Column(name = "tanggal_awal", nullable = false)
    private Date TanggalAwal;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.DATE)
    @Column(name = "tanggal_akhir", nullable = false)
    private Date TanggalAkhir;

    @Column(name = "lama_cuti", nullable = false)
    private Integer LamaCuti;

    @ManyToOne
    @JoinColumn(name = "karyawan_id", insertable = false, updatable = false)
    private Karyawan karyawan;

    @Column(name = "karyawan_id", nullable = false)
    private Long KaryawanId;

    @ManyToOne
    @JoinColumn(name = "jenis_cuti_id", insertable = false, updatable = false)
    private JenisCuti jenisCuti;

    @Column(name = "jenis_cuti_id", nullable = false)
    private Long JenisCutiId;

    @ManyToOne
    @JoinColumn(name = "status_id", insertable = false, updatable = false)
    private Status status;

    @Column(name = "status_id", nullable = false)
    private Long StatusId = 1L;


    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Date getTanggalAwal() {
        return TanggalAwal;
    }

    public void setTanggalAwal(Date tanggalAwal) {
        TanggalAwal = tanggalAwal;
    }

    public Date getTanggalAkhir() {
        return TanggalAkhir;
    }

    public void setTanggalAkhir(Date tanggalAkhir) {
        TanggalAkhir = tanggalAkhir;
    }

    public Integer getLamaCuti() {
        TimeUnit timeUnit = TimeUnit.DAYS;
        long mili = 86400000;
        long bedaHari = (TanggalAkhir.getTime() - TanggalAwal.getTime())/mili;
//        System.out.println("==========================="+bedaHari);
        return (int) (bedaHari);
//        return LamaCuti;
    }

    public void setLamaCuti(Integer lamaCuti) {
        LamaCuti = lamaCuti;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getStatusId() {
        return StatusId;
    }

    public void setStatusId(Long statusId) {
        StatusId = statusId;
    }

    public JenisCuti getJenisCuti() {
        return jenisCuti;
    }

    public void setJenisCuti(JenisCuti jenisCuti) {
        this.jenisCuti = jenisCuti;
    }

    public Long getJenisCutiId() {
        return JenisCutiId;
    }

    public void setJenisCutiId(Long jenisCutiId) {
        JenisCutiId = jenisCutiId;
    }

    public Karyawan getKaryawan() {
        return karyawan;
    }

    public void setKaryawan(Karyawan karyawan) {
        this.karyawan = karyawan;
    }

    public Long getKaryawanId() {
        return KaryawanId;
    }

    public void setKaryawanId(Long karyawanId) {
        KaryawanId = karyawanId;
    }
}
