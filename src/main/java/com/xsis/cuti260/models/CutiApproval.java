package com.xsis.cuti260.models;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "cuti_approval")
@Where(clause = "is_delete = false")
public class CutiApproval extends Common {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long Id;

    @OneToOne
    @JoinColumn(name = "cuti_id", insertable = false, updatable = false)
    private Cuti cuti;

    @Column(name = "cuti_id", nullable = false)
    private Long CutiId;

    @ManyToOne
    @JoinColumn(name = "status_id", insertable = false, updatable = false)
    private Status status;

    @Column(name = "status_id", nullable = false)
    private Long StatusId;

    @Column(name = "keterangan", nullable = false)
    private String Keterangan;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Cuti getCuti() {
        return cuti;
    }

    public void setCuti(Cuti cuti) {
        this.cuti = cuti;
    }

    public Long getCutiId() {
        return CutiId;
    }

    public void setCutiId(Long cutiId) {
        CutiId = cutiId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getStatusId() {
        return StatusId;
    }

    public void setStatusId(Long statusId) {
        StatusId = statusId;
    }

    public String getKeterangan() {
        return Keterangan;
    }

    public void setKeterangan(String keterangan) {
        Keterangan = keterangan;
    }
}
