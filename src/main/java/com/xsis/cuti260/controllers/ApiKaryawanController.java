package com.xsis.cuti260.controllers;

import com.xsis.cuti260.models.Karyawan;
import com.xsis.cuti260.models.Login;
import com.xsis.cuti260.repositories.KaryawanRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiKaryawanController {

    @Autowired
    private KaryawanRepo karyawanRepo;

    @PostMapping(value = "/karyawan")
    public ResponseEntity<Object> SaveKaryawan(@RequestBody Karyawan karyawan) {
        try {
            karyawan.setCreatedBy("Seno");
            karyawan.setCreatedOn(new Date());
            this.karyawanRepo.save(karyawan);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/karyawan")
    public ResponseEntity<Page<Karyawan>> GetAllKaryawan(@RequestParam Integer page, @RequestParam Integer size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Karyawan> karyawans = this.karyawanRepo.findAll(pageable);

            return new ResponseEntity<>(karyawans, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/karyawan/{id}")
    public ResponseEntity<List<Karyawan>> GetKaryawanById(@PathVariable("id") Long id) {
        try {
            Optional<Karyawan> karyawan = this.karyawanRepo.findById(id);
            if (karyawan.isPresent()) {
                ResponseEntity rest = new ResponseEntity(karyawan, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping(value = "/karyawan/{id}")
    public ResponseEntity<Object> UpdateKaryawan(@RequestBody Karyawan karyawan, @PathVariable("id") Long id) {
        try {
            Optional<Karyawan> karyawanData = this.karyawanRepo.findById(id);

            if (karyawanData.isPresent()) {
                // set id biar nanti nge replace data dengan id ini
                // karyawanData.get().setId(id);
                karyawanData.get().setNama(karyawan.getNama());
                karyawanData.get().setPassword(karyawan.getPassword());
                karyawanData.get().setEmail(karyawan.getEmail());
                karyawanData.get().setJenisKaryawanId(karyawan.getJenisKaryawanId());
                karyawanData.get().setModifieddBy("Seno");
                karyawanData.get().setModifiedOn(new Date());
                this.karyawanRepo.save(karyawanData.get());

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/karyawan/{id}")
    public ResponseEntity<Object> DeleteKaryawan(@PathVariable("id") Long id) {
        try {
            Optional<Karyawan> karyawanData = this.karyawanRepo.findById(id);
            if (karyawanData.isPresent()) {
                // System.out.println(categoryData.get().getCategoryName());
                karyawanData.get().setId(id);
                karyawanData.get().setDelete(true);
                this.karyawanRepo.save(karyawanData.get());

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }

        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/validate")
    public ResponseEntity<Object> ValidateKaryawan(@RequestBody Login login, HttpSession session) {
        try {
            Optional<Karyawan> karyawanData = this.karyawanRepo.findByEmailPassword(login.getEmail(), login.getPassword());
            if (karyawanData.isPresent()) {
                session.setAttribute("id", karyawanData.get().getId());
                session.setAttribute("jenisKaryawanId", karyawanData.get().getJenisKaryawanId());
                session.setAttribute("nama", karyawanData.get().getNama());
                return new ResponseEntity<>("success", HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/logout")
    public ResponseEntity<Object> LogoutKaryawan(HttpSession session) {
        try {

            if (session.getAttribute("id") != null && session.getAttribute("jenisKaryawanId") != null) {
                session.removeAttribute("id");
                session.removeAttribute("jenisKaryawanId");
                session.removeAttribute("nama");
                return new ResponseEntity<>("logout success", HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>("logout failed", HttpStatus.BAD_REQUEST);
        }
    }
}
