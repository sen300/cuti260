package com.xsis.cuti260.controllers;

import com.xsis.cuti260.models.JenisCuti;
import com.xsis.cuti260.repositories.JenisCutiRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class ApiJenisCutiController {
    @Autowired
    private JenisCutiRepo jenisCutiRepo;

    @PostMapping(value = "/jeniscuti")
    public ResponseEntity<Object> SaveJenisCuti(@RequestBody JenisCuti jenisCuti) {
        try {
            jenisCuti.setCreatedBy("Seno");
            jenisCuti.setCreatedOn(new Date());
            this.jenisCutiRepo.save(jenisCuti);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/jeniscuti")
    public ResponseEntity<List<JenisCuti>> GetAllJenisCuti() {
        try {
            List<JenisCuti> jenisCutis = this.jenisCutiRepo.findAll();
            return new ResponseEntity<>(jenisCutis, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/jeniscuti/{id}")
    public ResponseEntity<List<JenisCuti>> GetJenisCutiById(@PathVariable("id") Long id) {
        try {
            Optional<JenisCuti> jenisCuti = this.jenisCutiRepo.findById(id);
            if (jenisCuti.isPresent()) {
                ResponseEntity rest = new ResponseEntity(jenisCuti, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping(value = "/jeniscuti/{id}")
    public ResponseEntity<Object> UpdateJenisCuti(@RequestBody JenisCuti jenisCuti, @PathVariable("id") Long id) {
        try {
            Optional<JenisCuti> jenisCutiData = this.jenisCutiRepo.findById(id);

            if (jenisCutiData.isPresent()) {
                jenisCutiData.get().setNama(jenisCuti.getNama());
                jenisCutiData.get().setKeterangan(jenisCuti.getKeterangan());
                jenisCutiData.get().setModifieddBy("Seno");
                jenisCutiData.get().setModifiedOn(new Date());
                this.jenisCutiRepo.save(jenisCutiData.get());

                ResponseEntity rest = new ResponseEntity("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping(value = "/jeniscuti/{id}")
    public ResponseEntity<Object> DeleteJenisCuti(@PathVariable("id") Long id) {
        try {
            Optional<JenisCuti> jenisCutiData = this.jenisCutiRepo.findById(id);

            if (jenisCutiData.isPresent()) {
                jenisCutiData.get().setId(id);
                jenisCutiData.get().setDelete(true);
                jenisCutiData.get().setModifieddBy("Seno");
                jenisCutiData.get().setModifiedOn(new Date());
                this.jenisCutiRepo.save(jenisCutiData.get());

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
