package com.xsis.cuti260.controllers;

import com.xsis.cuti260.models.Status;
import com.xsis.cuti260.repositories.StatusRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class ApiStatusController {
    @Autowired
    private StatusRepo statusRepo;

    @PostMapping(value = "/status")
    public ResponseEntity<Object> SaveStatus(@RequestBody Status status) {
        try {
            status.setCreatedBy("Seno");
            status.setCreatedOn(new Date());
            this.statusRepo.save(status);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/status")
    public ResponseEntity<List<Status>> GetAllStatus() {
        try {
            List<Status> statuses = this.statusRepo.findAll();
            return new ResponseEntity<>(statuses, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/status/{id}")
    public ResponseEntity<List<Status>> GetStatusById(@PathVariable("id") Long id) {
        try {
            Optional<Status> status = this.statusRepo.findById(id);
            if (status.isPresent()) {
                ResponseEntity rest = new ResponseEntity(status, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping(value = "/status/{id}")
    public ResponseEntity<Object> UpdateStatus(@RequestBody Status status, @PathVariable("id") Long id) {
        try {
            Optional<Status> statusData = this.statusRepo.findById(id);

            if (statusData.isPresent()) {
                statusData.get().setNama(status.getNama());
                statusData.get().setKeterangan(status.getKeterangan());
                statusData.get().setModifieddBy("Seno");
                statusData.get().setModifiedOn(new Date());
                this.statusRepo.save(statusData.get());

                ResponseEntity rest = new ResponseEntity("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping(value = "/status/{id}")
    public ResponseEntity<Object> DeleteStatus(@PathVariable("id") Long id) {
        try {
            Optional<Status> statusData = this.statusRepo.findById(id);

            if (statusData.isPresent()) {
                statusData.get().setId(id);
                statusData.get().setDelete(true);
                statusData.get().setModifieddBy("Seno");
                statusData.get().setModifiedOn(new Date());
                this.statusRepo.save(statusData.get());

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
