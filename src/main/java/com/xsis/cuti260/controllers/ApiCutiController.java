package com.xsis.cuti260.controllers;

import com.xsis.cuti260.models.Cuti;
import com.xsis.cuti260.models.CutiApproval;
import com.xsis.cuti260.models.JenisCuti;
import com.xsis.cuti260.models.Karyawan;
import com.xsis.cuti260.repositories.CutiRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class ApiCutiController {
    @Autowired
    private CutiRepo cutiRepo;

    @PostMapping(value = "/cuti")
    public ResponseEntity<Object> SaveCuti(@RequestBody Cuti cuti, HttpSession session) {
        try {
            // System.out.println("================="+cuti.getKaryawan().getId());
            // cuti.getKaryawan().setSisaCuti(cuti.getKaryawan().getSisaCuti() -
            // cuti.getLamaCuti());
            Optional<Karyawan> karyawan = this.cutiRepo.findKaryawanById((Long) session.getAttribute("id"));

            // System.out.println("==============" + karyawan.get().getSisaCuti()
            // +"=========="+ cuti.getLamaCuti());

            if (karyawan.get().getSisaCuti() > cuti.getLamaCuti()) {
                int sisacuti = karyawan.get().getSisaCuti() - cuti.getLamaCuti();
                this.cutiRepo.updateSisaCutiKaryawan((Long) session.getAttribute("id"), sisacuti);

                cuti.setKaryawanId((Long) session.getAttribute("id"));
                cuti.setLamaCuti(cuti.getLamaCuti());
                cuti.setCreatedBy("Seno");
                cuti.setCreatedOn(new Date());
                this.cutiRepo.save(cuti);
                return new ResponseEntity<>("success", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("sisa cuti kurang/habis", HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/cuti")
    public ResponseEntity<Page<Cuti>> GetAllCuti(@RequestParam Integer page, @RequestParam Integer size,
            HttpSession session) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Cuti> cutis = this.cutiRepo.findAllCutiById((Long) session.getAttribute("id"), pageable);
            return new ResponseEntity<>(cutis, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/cuti/{id}")
    public ResponseEntity<List<Cuti>> GetCutiById(@PathVariable("id") Long id) {
        try {
            Optional<Cuti> cuti = this.cutiRepo.findById(id);
            if (cuti.isPresent()) {
                ResponseEntity rest = new ResponseEntity(cuti, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping(value = "/cuti/{id}")
    public ResponseEntity<Object> UpdateCuti(@RequestBody Cuti cuti, @PathVariable("id") Long id) {
        try {
            Optional<Cuti> cutiData = this.cutiRepo.findById(id);

            if (cutiData.isPresent()) {

                cutiData.get().setTanggalAwal(cuti.getTanggalAwal());
                cutiData.get().setTanggalAkhir(cuti.getTanggalAkhir());
                cutiData.get().setLamaCuti(cuti.getLamaCuti());
                // cutiData.get().setStatusId(1L);
                cutiData.get().setModifieddBy("Seno");
                cutiData.get().setModifiedOn(new Date());
                this.cutiRepo.save(cutiData.get());

                ResponseEntity rest = new ResponseEntity("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping(value = "/cuti/{id}")
    public ResponseEntity<Object> DeleteCuti(@PathVariable("id") Long id, HttpSession session) {
        try {
            Optional<Cuti> cutiData = this.cutiRepo.findById(id);

            Optional<Karyawan> karyawan = this.cutiRepo.findKaryawanById((Long) session.getAttribute("id"));
            if (cutiData.isPresent()) {
                int sisacuti = karyawan.get().getSisaCuti() + cutiData.get().getLamaCuti();
                // System.out.println("====================="+sisacuti);
                this.cutiRepo.updateSisaCutiKaryawan((Long) session.getAttribute("id"), sisacuti);

                cutiData.get().setId(id);
                cutiData.get().setDelete(true);
                cutiData.get().setModifieddBy("Seno");
                cutiData.get().setModifiedOn(new Date());
                this.cutiRepo.save(cutiData.get());

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/cuti/max/id")
    public ResponseEntity<Long> GetCutiMaxId() {
        try {
            Long cuti = this.cutiRepo.GetMaxCutiId();
            System.out.print(cuti);
            return new ResponseEntity<>(cuti, HttpStatus.OK);
        }

        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping(value = "/cuti/status/{id}")
    public ResponseEntity<Object> UpdateStatusCuti(@RequestBody Cuti cuti, @PathVariable("id") Long id) {
        try {
            Optional<Cuti> cutiData = this.cutiRepo.findById(id);

            if (cutiData.isPresent()) {

                cutiData.get().setStatusId(cuti.getStatusId());
                cutiData.get().setModifieddBy("Seno");
                cutiData.get().setModifiedOn(new Date());
                this.cutiRepo.save(cutiData.get());

                ResponseEntity rest = new ResponseEntity("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
