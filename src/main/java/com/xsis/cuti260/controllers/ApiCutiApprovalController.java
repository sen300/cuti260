package com.xsis.cuti260.controllers;

import com.xsis.cuti260.models.Cuti;
import com.xsis.cuti260.models.CutiApproval;
import com.xsis.cuti260.models.JenisCuti;
import com.xsis.cuti260.models.Karyawan;
import com.xsis.cuti260.repositories.CutiApprovalRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class ApiCutiApprovalController {
    @Autowired
    private CutiApprovalRepo cutiApprovalRepo;

    @PostMapping(value = "/cutiapproval")
    public ResponseEntity<Object> SaveCutiApproval(@RequestBody CutiApproval cutiApproval) {
        try {
            cutiApproval.setCreatedBy("Seno");
            cutiApproval.setCreatedOn(new Date());
            this.cutiApprovalRepo.save(cutiApproval);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/cutiapproval")
    public ResponseEntity<List<CutiApproval>> GetAllCutiApproval() {
        try {
            List<CutiApproval> cutiApprovals = this.cutiApprovalRepo.findAll();
            return new ResponseEntity<>(cutiApprovals, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/cutiapproval/{id}")
    public ResponseEntity<List<CutiApproval>> GetCutiApprovalById(@PathVariable("id") Long id) {
        try {
            Optional<CutiApproval> cutiApproval = this.cutiApprovalRepo.findById(id);
            if (cutiApproval.isPresent()) {
                ResponseEntity rest = new ResponseEntity(cutiApproval, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping(value = "/cutiapproval/{id}")
    public ResponseEntity<Object> UpdateCutiApproval(@RequestBody CutiApproval cutiApproval, @PathVariable("id") Long id, HttpSession session) {
        //try {
            Optional<CutiApproval> cutiApprovalData = this.cutiApprovalRepo.findById(id);

            Optional<Cuti> cuti = this.cutiApprovalRepo.findCutiById(cutiApprovalData.get().getCutiId());
            Optional<Karyawan> karyawan = this.cutiApprovalRepo.findKaryawanById(cuti.get().getKaryawanId());

            //System.out.println("==================="+cutiApproval.getStatusId());

            if (cutiApprovalData.isPresent()) {
                if (cutiApproval.getStatusId() == 3) {
                    int sisacuti = karyawan.get().getSisaCuti() + cuti.get().getLamaCuti();
                    //System.out.println("====================="+sisacuti);
                    this.cutiApprovalRepo.updateSisaCutiKaryawan((Long) session.getAttribute("id"), sisacuti);
                }

                cutiApprovalData.get().setStatusId(cutiApproval.getStatusId());
                cutiApprovalData.get().setKeterangan(cutiApproval.getKeterangan());
                cutiApprovalData.get().setModifieddBy("Seno");
                cutiApprovalData.get().setModifiedOn(new Date());
                this.cutiApprovalRepo.save(cutiApprovalData.get());

                ResponseEntity rest = new ResponseEntity("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        //} catch (Exception e) {
        //    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        //}
    }

    @DeleteMapping(value = "/cutiapproval/{id}")
    public ResponseEntity<Object> DeleteCutiApproval(@PathVariable("id") Long id) {
        try {
            Optional<CutiApproval> cutiApprovalData = this.cutiApprovalRepo.findById(id);

            if (cutiApprovalData.isPresent()) {
                cutiApprovalData.get().setId(id);
                cutiApprovalData.get().setDelete(true);
                cutiApprovalData.get().setModifieddBy("Seno");
                cutiApprovalData.get().setModifiedOn(new Date());
                this.cutiApprovalRepo.save(cutiApprovalData.get());

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/cutiapproval/cuti/{id}")
    public ResponseEntity<List<CutiApproval>> GetAllCutiApprovalByCutiId(@PathVariable("id") Long id) {
        System.out.println("============Tes");
        try{
            Optional<CutiApproval> cutiApproval = this.cutiApprovalRepo.GetCutiApprovalByCutiId(id);
            System.out.println("=============="+cutiApproval.get().getId());
            if (cutiApproval.isPresent()) {

                ResponseEntity rest = new ResponseEntity(cutiApproval, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/cutiapproval/status")
    public ResponseEntity<Page<CutiApproval>> GetAllCutiApprovalByStatusId(@RequestParam Integer page, @RequestParam Integer size) {
        //System.out.println("============Tes");
        try{
            Pageable pageable = PageRequest.of(page, size);
            Page<CutiApproval> cutiApprovals = this.cutiApprovalRepo.findAllByStatusId(pageable);
            return new ResponseEntity<>(cutiApprovals, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


}
