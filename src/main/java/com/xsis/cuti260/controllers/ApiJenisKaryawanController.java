package com.xsis.cuti260.controllers;

import com.xsis.cuti260.models.JenisKaryawan;
import com.xsis.cuti260.repositories.JenisKaryawanRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class ApiJenisKaryawanController {
    @Autowired
    private JenisKaryawanRepo jenisKaryawanRepo;

    @PostMapping(value = "/jeniskaryawan")
    public ResponseEntity<Object> SaveJenisKaryawan(@RequestBody JenisKaryawan jenisKaryawan) {
        try {
            jenisKaryawan.setCreatedBy("Seno");
            jenisKaryawan.setCreatedOn(new Date());
            this.jenisKaryawanRepo.save(jenisKaryawan);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/jeniskaryawan")
    public ResponseEntity<List<JenisKaryawan>> GetAllJenisKaryawan() {
        try {
            List<JenisKaryawan> jenisKaryawans = this.jenisKaryawanRepo.findAll();
            return new ResponseEntity<>(jenisKaryawans, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/jeniskaryawan/{id}")
    public ResponseEntity<List<JenisKaryawan>> GetJenisKaryawanById(@PathVariable("id") Long id) {
        try {
            Optional<JenisKaryawan> jenisKaryawan = this.jenisKaryawanRepo.findById(id);
            if (jenisKaryawan.isPresent()) {
                ResponseEntity rest = new ResponseEntity(jenisKaryawan, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping(value = "/jeniskaryawan/{id}")
    public ResponseEntity<Object> UpdateJenisKaryawan(@RequestBody JenisKaryawan jenisKaryawan,
            @PathVariable("id") Long id) {
        try {
            Optional<JenisKaryawan> jenisKaryawanData = jenisKaryawanRepo.findById(id);

            if (jenisKaryawanData.isPresent()) {
                jenisKaryawanData.get().setNama(jenisKaryawan.getNama());
                jenisKaryawanData.get().setKeterangan(jenisKaryawan.getKeterangan());
                jenisKaryawanData.get().setModifieddBy("Seno");
                jenisKaryawanData.get().setModifiedOn(new Date());
                this.jenisKaryawanRepo.save(jenisKaryawanData.get());

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping(value = "/jeniskaryawan/{id}")
    public ResponseEntity<Object> DeleteJenisKaryawan(@PathVariable("id") Long id) {
        try {
            Optional<JenisKaryawan> jenisKaryawanData = jenisKaryawanRepo.findById(id);

            if (jenisKaryawanData.isPresent()) {
                jenisKaryawanData.get().setId(id);
                jenisKaryawanData.get().setDelete(true);
                jenisKaryawanData.get().setModifieddBy("Seno");
                jenisKaryawanData.get().setModifiedOn(new Date());
                this.jenisKaryawanRepo.save(jenisKaryawanData.get());

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }
}
