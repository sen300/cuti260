package com.xsis.cuti260.repositories;

import com.xsis.cuti260.models.Cuti;
import com.xsis.cuti260.models.Karyawan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface CutiRepo extends JpaRepository<Cuti, Long> {
    @Query("SELECT MAX(id) AS maxid FROM Cuti ")
    Long GetMaxCutiId();

    @Query("SELECT c FROM Cuti c WHERE karyawan_id = ?1")
    Page<Cuti> findAllCutiById(Long id, Pageable pageable);

    @Query("SELECT c FROM Karyawan c WHERE Id = ?1")
    Optional<Karyawan> findKaryawanById(Long id);

    @Modifying
    @Transactional
    @Query("UPDATE Karyawan k SET k.SisaCuti = ?2 WHERE Id = ?1")
    void updateSisaCutiKaryawan(Long id, Integer lamaCuti);
}
