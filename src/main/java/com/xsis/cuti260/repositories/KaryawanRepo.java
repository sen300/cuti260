package com.xsis.cuti260.repositories;

import com.xsis.cuti260.models.CutiApproval;
import com.xsis.cuti260.models.Karyawan;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface KaryawanRepo extends JpaRepository<Karyawan, Long> {
    @Query(value = "SELECT * FROM Karyawan e WHERE e.email = ?1 AND e.password = ?2", nativeQuery = true)       // using @query
    Optional<Karyawan> findByEmailPassword(String email, String password);
}
