package com.xsis.cuti260.repositories;

import com.xsis.cuti260.models.Cuti;
import com.xsis.cuti260.models.CutiApproval;
import com.xsis.cuti260.models.Karyawan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface CutiApprovalRepo extends JpaRepository<CutiApproval, Long> {
    @Query(value = "SELECT * FROM CutiApproval WHERE cuti_id = ?1", nativeQuery = true)
    Optional<CutiApproval> GetCutiApprovalByCutiId(Long id);

    @Query(value = "SELECT e from CutiApproval e where e.StatusId = 1")       // using @query
    Page<CutiApproval> findAllByStatusId(Pageable pageable);

    @Query("SELECT c FROM Cuti c WHERE Id = ?1")
    Optional<Cuti> findCutiById(Long id);

    @Query("SELECT c FROM Karyawan c WHERE Id = ?1")
    Optional<Karyawan> findKaryawanById(Long id);

    @Modifying
    @Transactional
    @Query("UPDATE Karyawan k SET k.SisaCuti = ?2 WHERE Id = ?1")
    void updateSisaCutiKaryawan(Long id, Integer lamaCuti);
}
